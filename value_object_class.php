<?php
class ValueObject
{
    private $red;
    private $green;
    private $blue;

    public function __construct(int $red, int $green, int $blue) {
        try {
            $this->setRed($red);
            $this->setGreen($green);
            $this->setBlue($blue);
        } catch (Exception $colorError) {
            echo 'Invalid color number!';
            $colorError->getMessage();
            die();
        }
    }

    public function getRed(): int {
        return $this->red;
    }

    public function getGreen(): int {
        return $this->green;
    }

    public function getBlue(): int {
        return $this->blue;
    }

    private function setRed(int $red) {
        if($this->numberScope($red)) {
            return $this->red = $red;
        } else {
            throw new Exception('Invalid color number!');  
        } 
    }

    private function setGreen(int $green) {
        if($this->numberScope($green)) {
            return $this->green = $green;
        } else {
            throw new Exception('Invalid color number!');  
        } 
    }

    private function setBlue(int $blue) {
        if($this->numberScope($blue)) {
            return $this->blue = $blue;
        } else {
            throw new Exception('Invalid color number!');  
        } 
    }

    public function numberScope($color) {
        if(0 <= $color && $color <= 255) {
            return true;
        } else {
            return false;
        } 
    }

    public function equals(ValueObject $color): bool {
        return $color == $this;
    }

    public static function random(): ValueObject {
        return new ValueObject(rand(0,255), rand(0,255), rand(0,255));
    }

    public function mix(ValueObject $color): ValueObject {
        $averageResultRed = ($this->red + $color->red) / 2;
        $averageResultGreen = ($this->green + $color->green) / 2;
        $averageResultBlue = ($this->blue + $color->blue) / 2;
        return new ValueObject($this->setRed($averageResultRed), $this->setGreen($averageResultGreen), $this->setBlue($averageResultBlue)); 
    }
}
?>