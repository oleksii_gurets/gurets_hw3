<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/value_object_class.php';
/*Создаём объект цвета*/
$color = new ValueObject(10, 50, 200);
echo '<pre>';
print_r($color); //Выводим полученный объект в виде массива данных

/*Выводим значения полей полученного RGB оъекта через методы -геттеры */
echo '<br/>';
echo $color->getRed();
echo '<br/>';
echo $color->getGreen();
echo '<br/>';
echo $color->getBlue();
echo '<br/>';

$secondColor = new ValueObject(100, 150, 250);
echo '<pre>';
print_r($secondColor); //Выводим второй полученный RGB объект в виде массива данных

var_dump($color->equals($color)); //Вернет TRUE, сравнение объекта с одинаковыми значениями полей
var_dump($color->equals($secondColor)); //Вернет FALSE, сравнение объектов с разными значениями полей

/*С помощью статического метода получаем объект с рандомными значениями диапозона 0...255 */
$randomColor = ValueObject::random(); 
echo '<pre>';
print_r($randomColor); 
/*Выводим полученный объект в виде массива данных, 
значения свойств будут генерироваться заново при каждом обновлении страницы*/

//Получаем новый объект, смешивая первый объект со вторым(вычисляем среднее значение для каждого свойства цвета)
$mixedColor = $color->mix($secondColor); 
echo '<pre>';
print_r($mixedColor); 

/*Здесь ловим исключение с выводом сообщения, при попытке передать в метод mix новый экземпляр класса
со значениями полей, выходящими за диапозон 0...255*/
$mixedColor2 = $secondColor->mix(new ValueObject(300, -100, 300));
?>